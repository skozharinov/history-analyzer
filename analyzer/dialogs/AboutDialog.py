from PySide2 import QtCore, QtWidgets


class AboutDialog(QtWidgets.QDialog):
    def __init__(self, parent: QtWidgets.QWidget = None):
        super().__init__(parent)

        icon = QtWidgets.QApplication.windowIcon()
        iconPixmap = icon.pixmap(icon.actualSize(QtCore.QSize(128, 128)))
        title = self.tr('History Analyzer')
        description = self.tr('Plotter for Google Chrome browser history data')
        version = QtWidgets.QApplication.applicationVersion()
        copyright_ = self.tr('Copyright (c) 2019 Sergey Kozharinov')

        self.iconLabel = QtWidgets.QLabel()
        self.iconLabel.setPixmap(iconPixmap)
        # noinspection PyUnresolvedReferences
        self.iconLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.titleLabel = QtWidgets.QLabel(self.tr('<h1><b>%s</b></h1>') % title)
        # noinspection PyUnresolvedReferences
        self.titleLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.descriptionLabel = QtWidgets.QLabel(self.tr('<h4>%s</h4>') % description)
        # noinspection PyUnresolvedReferences
        self.descriptionLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.versionLabel = QtWidgets.QLabel(self.tr('Version: %s') % version)
        # noinspection PyUnresolvedReferences
        self.versionLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.copyrightLabel = QtWidgets.QLabel(copyright_)
        # noinspection PyUnresolvedReferences
        self.copyrightLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.dialogLayout = QtWidgets.QVBoxLayout()
        self.dialogLayout.addWidget(self.iconLabel)
        self.dialogLayout.addWidget(self.titleLabel)
        self.dialogLayout.addWidget(self.descriptionLabel)
        self.dialogLayout.addWidget(self.versionLabel)
        self.dialogLayout.addWidget(self.copyrightLabel)

        self.setLayout(self.dialogLayout)
        self.setWindowTitle(self.tr('About'))
