import typing

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCharts import QtCharts


class ExportDialog(QtWidgets.QDialog):
    def __init__(self, chartViews: typing.List[QtCharts.QChartView], parent: QtWidgets.QWidget = None) -> None:
        super().__init__(parent)
        self.chartViews = chartViews

        self.fileEdit = QtWidgets.QLineEdit()
        self.fileEdit.setPlaceholderText(self.tr('Output file path'))
        self.fileEdit.textChanged.connect(self.onLineEditChanged)

        self.fileButton = QtWidgets.QPushButton(self.tr('Select'))
        self.fileButton.clicked.connect(self.onFileButtonClicked)

        self.fileLayout = QtWidgets.QHBoxLayout()
        self.fileLayout.addWidget(self.fileEdit)
        self.fileLayout.addWidget(self.fileButton)

        self.fileGroup = QtWidgets.QGroupBox(self.tr('File'))
        self.fileGroup.setLayout(self.fileLayout)

        self.chartBox = QtWidgets.QComboBox()
        self.chartBox.addItems([x.chart().title() for x in self.chartViews])

        self.chartLayout = QtWidgets.QHBoxLayout()
        self.chartLayout.addWidget(self.chartBox)

        self.chartGroup = QtWidgets.QGroupBox(self.tr('Chart'))
        self.chartGroup.setLayout(self.chartLayout)

        self.sizeWidthValidator = QtGui.QIntValidator(1, int(1e9))

        self.sizeWidth = QtWidgets.QLineEdit()
        self.sizeWidth.setPlaceholderText(self.tr('Width, px'))
        self.sizeWidth.setValidator(self.sizeWidthValidator)
        self.sizeWidth.textChanged.connect(self.onLineEditChanged)

        self.sizeX = QtWidgets.QLabel('x')

        self.sizeHeightValidator = QtGui.QIntValidator(1, int(1e9))

        self.sizeHeight = QtWidgets.QLineEdit()
        self.sizeHeight.setPlaceholderText(self.tr('Height, px'))
        self.sizeHeight.setValidator(self.sizeHeightValidator)
        self.sizeHeight.textChanged.connect(self.onLineEditChanged)

        self.sizeLayout = QtWidgets.QHBoxLayout()
        self.sizeLayout.addWidget(self.sizeWidth)
        self.sizeLayout.addWidget(self.sizeX)
        self.sizeLayout.addWidget(self.sizeHeight)

        self.sizeGroup = QtWidgets.QGroupBox(self.tr('Size'))
        self.sizeGroup.setLayout(self.sizeLayout)

        self.antialiasingCheckbox = QtWidgets.QCheckBox(self.tr('Chart antialiasing'))
        self.antialiasingCheckbox.setChecked(True)

        self.optionsLayout = QtWidgets.QHBoxLayout()
        self.optionsLayout.addWidget(self.antialiasingCheckbox)

        self.optionsGroup = QtWidgets.QGroupBox(self.tr('Options'))
        self.optionsGroup.setLayout(self.optionsLayout)

        self.exportButton = QtWidgets.QPushButton(self.tr('Export'))
        self.exportButton.setEnabled(False)
        self.exportButton.clicked.connect(self.onExportButtonClicked)

        self.statusLabel = QtWidgets.QLabel()
        # noinspection PyUnresolvedReferences
        self.statusLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        self.dialogLayout = QtWidgets.QVBoxLayout()
        self.dialogLayout.addWidget(self.fileGroup)
        self.dialogLayout.addWidget(self.chartGroup)
        self.dialogLayout.addWidget(self.sizeGroup)
        self.dialogLayout.addWidget(self.optionsGroup)
        self.dialogLayout.addWidget(self.exportButton)
        self.dialogLayout.addWidget(self.statusLabel)

        self.setLayout(self.dialogLayout)

    def onFileButtonClicked(self) -> None:
        if self.fileEdit.text():
            path = self.fileEdit.text()
        else:
            directory = QtCore.QStandardPaths.writableLocation(QtCore.QStandardPaths.StandardLocation.PicturesLocation)
            path = QtCore.QDir(directory).filePath('chart.png')
        filter_ = self.tr('Images (*.jpeg, *.jpg, *.png)')
        filename = QtWidgets.QFileDialog.getSaveFileName(self, self.tr('Export Chart'), path, filter_)[0]
        if filename:
            self.fileEdit.setText(filename)

    def onExportButtonClicked(self) -> None:
        charts = [x.chart() for x in self.chartViews]
        chartToExport = charts[[x.title() for x in charts].index(self.chartBox.currentText())]

        chartToRender = chartToExport.__class__()
        chartToRender.setCustomData(chartToExport.customData)

        size = QtCore.QSize(int(self.sizeWidth.text()), int(self.sizeHeight.text()))

        view = QtCharts.QChartView()
        view.setChart(chartToRender)
        view.setFixedSize(size)
        # noinspection PyUnresolvedReferences
        view.setAttribute(QtCore.Qt.WidgetAttribute.WA_DontShowOnScreen)
        view.show()

        self.exportButton.setEnabled(False)
        self.statusLabel.setText(self.tr('Exporting...'))

        if self.renderAndSaveWidget(view):
            self.statusLabel.setText(self.tr('Exported successfully!'))
        else:
            self.statusLabel.setText(self.tr('Export failed'))

        self.exportButton.setEnabled(True)

    def onLineEditChanged(self) -> None:
        if self.fileEdit.text() and self.sizeWidth.text() and self.sizeHeight.text():
            self.exportButton.setEnabled(True)
        else:
            self.exportButton.setEnabled(False)

    def renderAndSaveWidget(self, widget: QtWidgets.QWidget) -> bool:
        pixmap = QtGui.QPixmap(widget.size())
        # noinspection PyUnresolvedReferences
        pixmap.fill(QtCore.Qt.GlobalColor.transparent)

        painter = QtGui.QPainter(pixmap)
        painter.setRenderHint(QtGui.QPainter.RenderHint.Antialiasing, self.antialiasingCheckbox.isChecked())
        widget.render(painter)
        painter.end()

        return pixmap.save(self.fileEdit.text())
