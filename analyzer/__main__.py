import sys

from PySide2 import QtCore, QtGui, QtWidgets

import analyzer

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    # noinspection PyProtectedMember
    resourceDir = QtCore.QDir(sys._MEIPASS) if hasattr(sys, '_MEIPASS') else QtCore.QFileInfo(__file__).dir()
    lName = QtCore.QLocale.system().name()

    qtTranslator = QtCore.QTranslator()
    qtTranslator.load('qt_' + lName, QtCore.QLibraryInfo.location(QtCore.QLibraryInfo.LibraryLocation.TranslationsPath))
    app.installTranslator(qtTranslator)

    appTranslator = QtCore.QTranslator()
    appTranslator.load('translations/tr_' + lName, resourceDir.absolutePath())
    app.installTranslator(appTranslator)

    title = QtCore.QCoreApplication.translate('QCoreApplication', 'History Analyzer')
    icon = QtGui.QIcon(resourceDir.absoluteFilePath('icons/icon.png'))
    version = QtCore.QVersionNumber(1, 3, 2).toString()

    app.setApplicationName(title)
    app.setWindowIcon(icon)
    app.setApplicationVersion(version)

    analyzer = analyzer.MainWindow()
    analyzer.show()

    sys.exit(app.exec_())
