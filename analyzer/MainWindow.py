import enum
import typing

from PySide2 import QtCore, QtGui, QtWidgets
from PySide2.QtCharts import QtCharts

from .charts import DailyChart, DaysChart, DomainsChart, HoursChart, MonthsChart, TransitionsChart, WeekdaysChart, \
    YearsChart
from .dialogs import AboutDialog, ExportDialog
from .entities import HistoryRecord
from .helpers import ChartDataGenerator, JsonParser
from .models import HistoryTableModel


class MainWindow(QtWidgets.QMainWindow):
    recordsCount: int
    uiMode: 'MainWindow.Mode'
    usedThreads: typing.List[QtCore.QThread]

    icon: QtGui.QIcon
    helpLabel: QtWidgets.QLabel
    infoLabel: QtWidgets.QLabel
    progressBar: QtWidgets.QProgressBar

    openAction: QtWidgets.QAction
    closeAction: QtWidgets.QAction
    showingChartsSelected: QtWidgets.QAction
    showingHistorySelected: QtWidgets.QAction
    uiModeActionGroup: QtWidgets.QActionGroup
    exportAction: QtWidgets.QAction
    exitAction: QtWidgets.QAction

    aboutAction: QtWidgets.QAction
    aboutQtAction: QtWidgets.QAction

    fileMenu: QtWidgets.QMenu
    viewMenu: QtWidgets.QMenu
    helpMenu: QtWidgets.QMenu

    windowFrame: QtWidgets.QFrame
    windowFrameLayout: QtWidgets.QStackedLayout

    scrollArea: QtWidgets.QScrollArea
    scrollAreaFrame: QtWidgets.QFrame
    scrollAreaFrameLayout: QtWidgets.QGridLayout

    historyTable: QtWidgets.QTableView

    transitionsChartView: QtCharts.QChartView
    domainsChartView: QtCharts.QChartView
    dailyChartView: QtCharts.QChartView
    hoursChartView: QtCharts.QChartView
    weekdaysChartView: QtCharts.QChartView
    daysChartView: QtCharts.QChartView
    monthsChartView: QtCharts.QChartView
    yearsChartView: QtCharts.QChartView

    def __init__(self) -> None:
        super().__init__()
        self.mode = None
        self.recordsCount = int()
        self.usedThreads = list()
        self.createActions()
        self.createMenus()
        self.createStatusBar()
        self.createWindowLayout()
        self.createHelpLabel()
        self.createScrollArea()
        self.createChartViews()
        self.createHistoryTable()
        self.centerAndResize()
        self.updateUI(MainWindow.Mode.ShowingHelp)

    class Mode(enum.Enum):
        ShowingHelp = enum.auto()
        ShowingCharts = enum.auto()
        ShowingHistory = enum.auto()
        GeneratingRecords = enum.auto()
        GeneratingCharts = enum.auto()

    def createWindowLayout(self) -> None:
        self.windowFrameLayout = QtWidgets.QStackedLayout()
        self.windowFrameLayout.setContentsMargins(0, 0, 0, 0)

        self.windowFrame = QtWidgets.QFrame()
        self.windowFrame.setLayout(self.windowFrameLayout)

        self.setStyleSheet('QFrame { border: 0px }')
        self.setCentralWidget(self.windowFrame)

    def createActions(self) -> None:
        self.openAction = QtWidgets.QAction(QtGui.QIcon.fromTheme('document-open'), self.tr('Open'), self)
        self.openAction.setShortcut(QtGui.QKeySequence.StandardKey.Open)
        self.openAction.triggered.connect(self.openFile)

        self.closeAction = QtWidgets.QAction(self.tr('Close'), self)
        self.closeAction.setShortcut(QtGui.QKeySequence.StandardKey.Close)
        self.closeAction.triggered.connect(self.closeFile)

        self.exportAction = QtWidgets.QAction(self.tr('Export chart'), self)
        self.exportAction.triggered.connect(self.exportChart)

        self.showingChartsSelected = QtWidgets.QAction(self.tr('Show charts'), self)
        self.showingChartsSelected.setCheckable(True)
        self.showingChartsSelected.setChecked(True)

        self.showingHistorySelected = QtWidgets.QAction(self.tr('Show history'), self)
        self.showingHistorySelected.setCheckable(True)

        self.uiModeActionGroup = QtWidgets.QActionGroup(self)
        self.uiModeActionGroup.addAction(self.showingChartsSelected)
        self.uiModeActionGroup.addAction(self.showingHistorySelected)
        self.uiModeActionGroup.setExclusive(True)
        self.uiModeActionGroup.triggered.connect(self.onUiModeSelected)

        self.exitAction = QtWidgets.QAction(QtGui.QIcon.fromTheme('application-exit'), self.tr('Exit'), self)
        self.exitAction.setShortcut(QtGui.QKeySequence.StandardKey.Quit)
        self.exitAction.triggered.connect(QtWidgets.QApplication.quit)

        appName = QtWidgets.QApplication.applicationDisplayName()
        self.aboutAction = QtWidgets.QAction(self.windowIcon(), self.tr('About %s') % appName, self)
        self.aboutAction.triggered.connect(self.showAboutDialog)

        self.aboutQtAction = QtWidgets.QAction(QtGui.QIcon.fromTheme('help-about'), self.tr('About Qt'), self)
        self.aboutQtAction.triggered.connect(self.showAboutQtMessage)

    def createHistoryTable(self) -> None:
        self.historyTable = QtWidgets.QTableView()
        self.historyTable.setModel(HistoryTableModel())
        self.historyTable.setWordWrap(False)
        self.historyTable.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectionBehavior.SelectRows)
        self.historyTable.horizontalHeader().setStretchLastSection(True)
        self.historyTable.verticalHeader().hide()
        self.windowFrameLayout.addWidget(self.historyTable)

    def createHelpLabel(self) -> None:
        description = self.tr('1. Go to <a href="https://takeout.google.com">takeout.google.com</a><br>' +
                              '2. Leave only <i>Chrome</i> data checked<br>' +
                              '3. Leave only <i>BrowserHistory</i> format checked in <i>Chrome</i> data<br>' +
                              '4. Extract <b>BrowserHistory.json</b> from received archive<br>' +
                              '5. Open <b>BrowserHistory.json</b> with this app')

        self.helpLabel = QtWidgets.QLabel(description)
        self.helpLabel.setOpenExternalLinks(True)
        # noinspection PyUnresolvedReferences
        self.helpLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.windowFrameLayout.addWidget(self.helpLabel)

    def createStatusBar(self) -> None:
        self.progressBar = QtWidgets.QProgressBar()
        self.statusBar().addPermanentWidget(self.progressBar)

        self.infoLabel = QtWidgets.QLabel()
        self.statusBar().addPermanentWidget(self.infoLabel)

    def createChartViews(self) -> None:
        def createView():
            view = QtCharts.QChartView()
            view.setRenderHint(QtGui.QPainter.RenderHint.Antialiasing)
            view.setMinimumHeight(400)
            return view

        self.transitionsChartView = createView()
        self.transitionsChartView.setChart(TransitionsChart())
        self.scrollAreaFrameLayout.addWidget(self.transitionsChartView, 0, 0, 1, 1)

        self.domainsChartView = createView()
        self.domainsChartView.setChart(DomainsChart())
        self.scrollAreaFrameLayout.addWidget(self.domainsChartView, 0, 1, 1, 1)

        self.dailyChartView = createView()
        self.dailyChartView.setChart(DailyChart())
        self.scrollAreaFrameLayout.addWidget(self.dailyChartView, 1, 0, 1, 2)

        self.hoursChartView = createView()
        self.hoursChartView.setChart(HoursChart())
        self.scrollAreaFrameLayout.addWidget(self.hoursChartView, 2, 0, 1, 1)

        self.weekdaysChartView = createView()
        self.weekdaysChartView.setChart(WeekdaysChart())
        self.scrollAreaFrameLayout.addWidget(self.weekdaysChartView, 2, 1, 1, 1)

        self.daysChartView = createView()
        self.daysChartView.setChart(DaysChart())
        self.scrollAreaFrameLayout.addWidget(self.daysChartView, 3, 0, 1, 2)

        self.monthsChartView = createView()
        self.monthsChartView.setChart(MonthsChart())
        self.scrollAreaFrameLayout.addWidget(self.monthsChartView, 4, 0, 1, 1)

        self.yearsChartView = createView()
        self.yearsChartView.setChart(YearsChart())
        self.scrollAreaFrameLayout.addWidget(self.yearsChartView, 4, 1, 1, 1)

    def createMenus(self) -> None:
        self.fileMenu = self.menuBar().addMenu(self.tr('File'))

        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.closeAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exportAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAction)

        self.viewMenu = self.menuBar().addMenu(self.tr('View'))
        self.viewMenu.addAction(self.showingChartsSelected)
        self.viewMenu.addAction(self.showingHistorySelected)

        self.helpMenu = self.menuBar().addMenu(self.tr('Help'))
        self.helpMenu.addAction(self.aboutAction)
        self.helpMenu.addAction(self.aboutQtAction)

    def createScrollArea(self) -> None:
        self.scrollArea = QtWidgets.QScrollArea()
        # noinspection PyUnresolvedReferences
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        # noinspection PyUnresolvedReferences
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff)

        self.scrollAreaFrame = QtWidgets.QFrame()
        self.scrollArea.setWidget(self.scrollAreaFrame)
        self.scrollArea.setWidgetResizable(True)

        self.scrollAreaFrameLayout = QtWidgets.QGridLayout()
        self.scrollAreaFrameLayout.setSpacing(0)
        self.scrollAreaFrameLayout.setContentsMargins(0, 0, 0, 0)
        self.scrollAreaFrame.setLayout(self.scrollAreaFrameLayout)

        self.windowFrameLayout.addWidget(self.scrollArea)

    def centerAndResize(self) -> None:
        screenGeometry = QtWidgets.QApplication.primaryScreen().availableGeometry()
        self.resize(screenGeometry.size() * 0.7)
        self.move(screenGeometry.center() - self.geometry().center())

    def onUiModeSelected(self) -> None:
        if self.showingChartsSelected.isChecked():
            self.updateUI(MainWindow.Mode.ShowingCharts)
        else:
            self.updateUI(MainWindow.Mode.ShowingHistory)

    def openFile(self) -> None:
        path = QtCore.QStandardPaths.locate(QtCore.QStandardPaths.StandardLocation.DocumentsLocation, str(),
                                            QtCore.QStandardPaths.LocateOption.LocateDirectory)
        format_ = self.tr('JSON (*.json)')
        filename = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open Takeout file'), path, format_)[0]

        if filename:
            oldFilename = self.windowFilePath()
            self.loadRawData(filename, oldFilename)

    def exportChart(self) -> int:
        dialog = ExportDialog(self.getAllChartViews(), self)
        return dialog.exec_()

    def closeFile(self) -> None:
        self.setWindowFilePath(str())
        self.recordsCount = int()
        self.updateUI(MainWindow.Mode.ShowingHelp)

    def showAboutDialog(self) -> int:
        dialog = AboutDialog(self)
        return dialog.exec_()

    def showAboutQtMessage(self) -> None:
        QtWidgets.QMessageBox.aboutQt(self)

    def loadRawData(self, filename: str, oldFilename: str) -> None:
        file = QtCore.QFile(filename)

        if not file.open(QtCore.QIODevice.OpenModeFlag.ReadOnly):
            self.showOpenFailureMessage(oldFilename)
            return

        raw = file.readAll()
        file.close()
        self.setWindowFilePath(filename)
        self.parseRawData(raw, oldFilename)

    def parseRawData(self, raw: QtCore.QByteArray, oldFilename: str) -> None:
        self.updateUI(MainWindow.Mode.GeneratingRecords)

        thread = QtCore.QThread()
        self.usedThreads.append(thread)

        parser = JsonParser(raw)
        parser.moveToThread(thread)
        thread.started.connect(parser.parse)
        thread.finished.connect(thread.deleteLater)

        def onParserFinished() -> None:
            thread.exit()

            if parser.parsed:
                self.recordsCount = len(parser.parsed)
                self.historyTable.model().setHistoryData(parser.parsed)
                self.generateChartsData(parser.parsed)
            else:
                self.showOpenFailureMessage(oldFilename)

        parser.progress.connect(self.progressBar.setValue)
        parser.finished.connect(onParserFinished)
        thread.start()

    def generateChartsData(self, data: typing.List[HistoryRecord]) -> None:
        self.updateUI(MainWindow.Mode.GeneratingCharts)

        thread = QtCore.QThread()
        self.usedThreads.append(thread)

        generator = ChartDataGenerator(data)
        generator.moveToThread(thread)
        thread.started.connect(generator.generate)
        thread.finished.connect(thread.deleteLater)

        def onGeneratorFinished():
            thread.exit()

            self.transitionsChartView.chart().setCustomData(generator.transitionsChartData)
            self.domainsChartView.chart().setCustomData(generator.domainsChartData)
            self.dailyChartView.chart().setCustomData(generator.dailyChartData)
            self.hoursChartView.chart().setCustomData(generator.hoursChartData)
            self.weekdaysChartView.chart().setCustomData(generator.weekdaysChartData)
            self.daysChartView.chart().setCustomData(generator.daysChartData)
            self.monthsChartView.chart().setCustomData(generator.monthsChartData)
            self.yearsChartView.chart().setCustomData(generator.yearsChartData)

            self.updateUI(MainWindow.Mode.ShowingCharts)

        generator.progress.connect(self.progressBar.setValue)
        generator.finished.connect(onGeneratorFinished)
        thread.start()

    def showOpenFailureMessage(self, oldFilename: str) -> None:
        if oldFilename:
            self.setWindowFilePath(oldFilename)
            self.updateUI(MainWindow.Mode.ShowingCharts)
        else:
            self.closeFile()

        box = QtWidgets.QMessageBox(self)
        box.setText(self.tr('Oops!'))
        box.setInformativeText(self.tr('Failed to load your data'))
        box.setIcon(QtWidgets.QMessageBox.Icon.Critical)
        spacer = QtWidgets.QSpacerItem(300, 0, QtWidgets.QSizePolicy.Policy.Minimum,
                                       QtWidgets.QSizePolicy.Policy.Expanding)
        layout = box.layout()
        # noinspection PyArgumentList
        layout.addItem(spacer, layout.rowCount(), 0, 1, layout.columnCount())
        box.show()

    def getAllChartViews(self) -> typing.List[QtCharts.QChartView]:
        return list(filter(lambda x: isinstance(x, QtCharts.QChartView), vars(self).values()))

    def updateUI(self, mode: Mode) -> None:
        def setShowingHelpMode() -> None:
            self.openAction.setEnabled(True)
            self.exportAction.setEnabled(False)
            self.closeAction.setEnabled(False)
            self.exitAction.setEnabled(True)
            self.uiModeActionGroup.setEnabled(False)

            self.windowFrame.show()
            self.windowFrameLayout.setCurrentWidget(self.helpLabel)

            self.infoLabel.clear()
            self.progressBar.reset()
            self.progressBar.hide()

        def setShowingChartsMode() -> None:
            self.openAction.setEnabled(True)
            self.exportAction.setEnabled(True)
            self.closeAction.setEnabled(True)
            self.exitAction.setEnabled(True)
            self.uiModeActionGroup.setEnabled(True)
            self.showingChartsSelected.setChecked(True)

            self.windowFrame.show()
            self.windowFrameLayout.setCurrentWidget(self.scrollArea)

            self.infoLabel.setText(self.tr('%d records in this file') % self.recordsCount)
            self.progressBar.reset()
            self.progressBar.hide()

        def setShowingHistoryMode() -> None:
            self.openAction.setEnabled(True)
            self.exportAction.setEnabled(True)
            self.closeAction.setEnabled(True)
            self.exitAction.setEnabled(True)
            self.uiModeActionGroup.setEnabled(True)
            self.showingHistorySelected.setChecked(True)

            self.windowFrame.show()
            self.windowFrameLayout.setCurrentWidget(self.historyTable)

            self.infoLabel.setText(self.tr('%d records in this file') % self.recordsCount)
            self.progressBar.reset()
            self.progressBar.hide()

        def setGeneratingRecordsMode() -> None:
            self.openAction.setEnabled(False)
            self.exportAction.setEnabled(False)
            self.closeAction.setEnabled(False)
            self.exitAction.setEnabled(False)
            self.uiModeActionGroup.setEnabled(False)

            self.windowFrame.hide()

            self.infoLabel.setText(self.tr('Parsing...'))
            self.progressBar.reset()
            self.progressBar.show()

        def setGeneratingChartsMode() -> None:
            self.openAction.setEnabled(False)
            self.exportAction.setEnabled(False)
            self.closeAction.setEnabled(False)
            self.exitAction.setEnabled(False)
            self.uiModeActionGroup.setEnabled(False)

            self.windowFrame.hide()

            self.infoLabel.setText(self.tr('Processing charts...'))
            self.progressBar.reset()
            self.progressBar.show()

        switcher = {
            MainWindow.Mode.ShowingHelp: setShowingHelpMode,
            MainWindow.Mode.ShowingCharts: setShowingChartsMode,
            MainWindow.Mode.ShowingHistory: setShowingHistoryMode,
            MainWindow.Mode.GeneratingRecords: setGeneratingRecordsMode,
            MainWindow.Mode.GeneratingCharts: setGeneratingChartsMode,
        }

        if self.mode is not mode:
            self.mode = mode
            switcher[mode]()
