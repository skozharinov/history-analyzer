import typing

from PySide2 import QtCore

from ..entities import HistoryRecord


class HistoryTableModel(QtCore.QAbstractTableModel):
    TIME_INDEX = 0
    URL_INDEX = 1
    TITLE_INDEX = 2

    historyData: typing.List[HistoryRecord]

    def __init__(self, historyData: typing.List[HistoryRecord] = None, parent: QtCore.QObject = None):
        super().__init__(parent)
        self.setHistoryData(historyData)

    def rowCount(self, parent: QtCore.QModelIndex = None) -> int:
        return len(self.historyData)

    def columnCount(self, parent: QtCore.QModelIndex = None) -> int:
        return 3

    # noinspection PyUnresolvedReferences
    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.ItemDataRole.DisplayRole) -> typing.Any:
        role = QtCore.Qt.ItemDataRole(role)

        if index.isValid():
            if role == QtCore.Qt.ItemDataRole.DisplayRole:
                if index.column() == HistoryTableModel.TIME_INDEX:
                    return self.historyData[index.row()].dateTime.toString(QtCore.Qt.DateFormat.ISODate)
                elif index.column() == HistoryTableModel.URL_INDEX:
                    return self.historyData[index.row()].url.url()
                elif index.column() == HistoryTableModel.TITLE_INDEX:
                    return self.historyData[index.row()].title

        return None

    # noinspection PyUnresolvedReferences
    def headerData(self, section: int, orientation: QtCore.Qt.Orientation,
                   role: int = QtCore.Qt.ItemDataRole.DisplayRole) -> typing.Any:
        role = QtCore.Qt.ItemDataRole(role)

        if role == QtCore.Qt.ItemDataRole.DisplayRole:
            if orientation is QtCore.Qt.Orientation.Horizontal:
                if section == HistoryTableModel.TIME_INDEX:
                    return self.tr('Time visited')
                elif section == HistoryTableModel.URL_INDEX:
                    return self.tr('URL')
                elif section == HistoryTableModel.TITLE_INDEX:
                    return self.tr('Title')

        return None

    def setHistoryData(self, historyData: typing.List[HistoryRecord] = None) -> None:
        historyData = list() if historyData is None else historyData
        self.historyData = historyData
        self.layoutChanged.emit()
