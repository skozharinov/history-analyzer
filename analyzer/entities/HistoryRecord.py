import dataclasses
import enum

from PySide2 import QtCore


@dataclasses.dataclass
class HistoryRecord:
    transition: 'HistoryRecord.PageTransition'
    dateTime: QtCore.QDateTime
    url: QtCore.QUrl
    faviconUrl: QtCore.QUrl
    title: str

    class PageTransition(enum.Enum):
        Link = 'LINK'
        Typed = 'TYPED'
        AutoBookmark = 'AUTO_BOOKMARK'
        AutoSubframe = 'AUTO_SUBFRAME'
        ManualSubframe = 'MANUAL_SUBFRAME'
        Generated = 'GENERATED'
        AutoToplevel = 'AUTO_TOPLEVEL'
        FormSubmit = 'FORM_SUBMIT'
        Reload = 'RELOAD'
        Keyword = 'KEYWORD'
        KeywordGenerated = 'KEYWORD_GENERATED'
