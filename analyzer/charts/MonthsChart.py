import typing

from PySide2 import QtCore, QtGui
from PySide2.QtCharts import QtCharts


class MonthsChart(QtCharts.QChart):
    customData: typing.List[typing.Tuple[int, int]]
    __series: QtCharts.QBarSeries
    __axisX: QtCharts.QBarCategoryAxis
    __axisY: QtCharts.QValueAxis

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.customData = list()
        self.setTitle(self.tr('Months'))
        self.setMargins(QtCore.QMargins(10, 10, 10, 10))
        self.legend().hide()

    def clear(self) -> None:
        if self.customData:
            self.customData = list()
            self.removeSeries(self.__series)
            self.removeAxis(self.__axisX)
            self.removeAxis(self.__axisY)

    def createAxes(self) -> None:
        self.__axisX = QtCharts.QBarCategoryAxis()
        months = [self.tr('Jan'), self.tr('Feb'), self.tr('Mar'), self.tr('Apr'), self.tr('May'), self.tr('Jun'),
                  self.tr('Jul'), self.tr('Aug'), self.tr('Sep'), self.tr('Oct'), self.tr('Nov'), self.tr('Dec')]
        self.__axisX.append(months)
        # noinspection PyUnresolvedReferences
        self.addAxis(self.__axisX, QtCore.Qt.AlignmentFlag.AlignBottom)
        self.__series.attachAxis(self.__axisX)

        self.__axisY = QtCharts.QValueAxis()
        self.__axisY.setTitleText(self.tr('Visits'))
        self.__axisY.setLabelFormat('%d')
        # noinspection PyUnresolvedReferences
        self.addAxis(self.__axisY, QtCore.Qt.AlignmentFlag.AlignLeft)
        self.__series.attachAxis(self.__axisY)
        self.__axisY.setTickCount(10)
        self.__axisY.setMinorTickCount(5)
        self.__axisY.applyNiceNumbers()

    def createSeries(self) -> None:
        self.__series = QtCharts.QBarSeries()
        bar = QtCharts.QBarSet(self.tr('Total'))
        bar.append([v for _, v in self.customData])
        self.__series.append(bar)
        self.__series.setBarWidth(0.9)
        # noinspection PyUnresolvedReferences
        bar.setColor(QtGui.QColor.fromRgb(103, 58, 183))
        self.addSeries(self.__series)

    def setCustomData(self, data: typing.List[typing.Tuple[int, int]]) -> None:
        self.clear()
        self.customData = data
        self.createSeries()
        self.createAxes()
