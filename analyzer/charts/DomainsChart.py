import typing

from PySide2 import QtCore, QtGui
from PySide2.QtCharts import QtCharts


class DomainsChart(QtCharts.QChart):
    customData: typing.List[typing.Tuple[str, int]]
    __series: QtCharts.QBarSeries
    __axisY: QtCharts.QValueAxis

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.customData = list()
        self.setTitle(self.tr('Domains'))
        self.setMargins(QtCore.QMargins(10, 10, 10, 10))
        # noinspection PyUnresolvedReferences
        self.legend().setAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

    def clear(self) -> None:
        if self.customData:
            self.customData = list()
            self.removeSeries(self.__series)
            self.removeAxis(self.__axisY)

    def createAxes(self) -> None:
        self.__axisY = QtCharts.QValueAxis()
        self.__axisY.setTitleText(self.tr('Visits'))
        self.__axisY.setTickCount(10)
        self.__axisY.setMinorTickCount(5)
        self.__axisY.setLabelFormat('%d')
        # noinspection PyUnresolvedReferences
        self.addAxis(self.__axisY, QtCore.Qt.AlignmentFlag.AlignLeft)
        self.__series.attachAxis(self.__axisY)
        self.__axisY.applyNiceNumbers()

    def createSeries(self) -> None:
        colors = (x for x in [
            QtGui.QColor.fromRgb(33, 150, 243),
            QtGui.QColor.fromRgb(244, 67, 54),
            QtGui.QColor.fromRgb(76, 175, 80),
            QtGui.QColor.fromRgb(255, 87, 34),
            QtGui.QColor.fromRgb(156, 39, 176),
            QtGui.QColor.fromRgb(3, 169, 244),
            QtGui.QColor.fromRgb(255, 235, 59),
            QtGui.QColor.fromRgb(0, 150, 136),
            QtGui.QColor.fromRgb(0, 188, 212),
            QtGui.QColor.fromRgb(103, 58, 183)
        ])
        self.__series = QtCharts.QBarSeries()
        for k, v in self.customData[:10]:
            barSet = QtCharts.QBarSet(k)
            barSet.append(v)
            barSet.setColor(colors.__next__())
            self.__series.append(barSet)
        self.__series.setBarWidth(0.9)
        self.addSeries(self.__series)

    def setCustomData(self, data: typing.List[typing.Tuple[str, int]]) -> None:
        self.clear()
        self.customData = data
        self.createSeries()
        self.createAxes()
