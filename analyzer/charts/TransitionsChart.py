import typing

from PySide2 import QtCore, QtGui
from PySide2.QtCharts import QtCharts

from ..entities import HistoryRecord


class TransitionsChart(QtCharts.QChart):
    customData: typing.List[typing.Tuple[HistoryRecord.PageTransition, int]]
    __series: QtCharts.QPieSeries

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.customData = list()
        self.setTitle(self.tr('Transitions'))
        self.setMargins(QtCore.QMargins(10, 10, 10, 10))
        # noinspection PyUnresolvedReferences
        self.legend().setAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

    def clear(self) -> None:
        if self.customData:
            self.customData = list()
            self.removeSeries(self.__series)

    def createSeries(self) -> None:
        TRANSITION_DATA = {
            HistoryRecord.PageTransition.Link.value:
                (self.tr('Link'), QtGui.QColor.fromRgb(0, 188, 212)),
            HistoryRecord.PageTransition.Typed.value:
                (self.tr('Typed'), QtGui.QColor.fromRgb(33, 150, 243)),
            HistoryRecord.PageTransition.AutoBookmark.value:
                (self.tr('Auto Bookmark'), QtGui.QColor.fromRgb(76, 175, 80)),
            HistoryRecord.PageTransition.AutoSubframe.value:
                (self.tr('Auto Subframe'), QtGui.QColor.fromRgb(255, 152, 0)),
            HistoryRecord.PageTransition.ManualSubframe.value:
                (self.tr('Manual Subframe'), QtGui.QColor.fromRgb(156, 39, 176)),
            HistoryRecord.PageTransition.Generated.value:
                (self.tr('Omnibar Generated'), QtGui.QColor.fromRgb(244, 67, 54)),
            HistoryRecord.PageTransition.AutoToplevel.value:
                (self.tr('Top Level'), QtGui.QColor.fromRgb(63, 81, 181)),
            HistoryRecord.PageTransition.FormSubmit.value:
                (self.tr('Form Submit'), QtGui.QColor.fromRgb(0, 150, 136)),
            HistoryRecord.PageTransition.Reload.value:
                (self.tr('Reload'), QtGui.QColor.fromRgb(255, 235, 59)),
            HistoryRecord.PageTransition.Keyword.value:
                (self.tr('Keyword'), QtGui.QColor.fromRgb(205, 220, 57)),
            HistoryRecord.PageTransition.KeywordGenerated.value:
                (self.tr('Keyword Generated'), QtGui.QColor.fromRgb(121, 85, 72))
        }
        self.__series = QtCharts.QPieSeries()
        self.__series.append([QtCharts.QPieSlice(k.value, v) for k, v in self.customData])
        # noinspection PyTypeChecker
        for slice_ in self.__series.slices():
            data = TRANSITION_DATA[slice_.label()]
            slice_.setLabel('%s (%d, %d%%)' % (data[0], slice_.value(), slice_.percentage() * 100))
            slice_.setColor(data[1])
        self.__series.setPieSize(0.8)
        self.addSeries(self.__series)

    def setCustomData(self, data: typing.List[typing.Tuple[HistoryRecord.PageTransition, int]]) -> None:
        self.clear()
        self.customData = data
        self.createSeries()
