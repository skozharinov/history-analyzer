import statistics
import typing

from PySide2 import QtCore, QtGui
from PySide2.QtCharts import QtCharts


class DailyChart(QtCharts.QChart):
    customData: typing.List[typing.Tuple[int, int]]
    __seriesDaily: QtCharts.QLineSeries
    __seriesMean: QtCharts.QLineSeries
    __seriesMedian: QtCharts.QLineSeries
    __axisX: QtCharts.QDateTimeAxis
    __axisY: QtCharts.QValueAxis

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.customData = list()
        self.setTitle(self.tr('Daily'))
        self.setMargins(QtCore.QMargins(10, 10, 10, 10))
        # noinspection PyUnresolvedReferences
        self.legend().setAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

    def clear(self) -> None:
        if self.customData:
            self.customData = list()
            self.removeSeries(self.__seriesDaily)
            self.removeSeries(self.__seriesMean)
            self.removeSeries(self.__seriesMedian)
            self.removeAxis(self.__axisX)
            self.removeAxis(self.__axisY)

    def createAxes(self) -> None:
        self.__axisX = QtCharts.QDateTimeAxis()
        self.__axisX.setTickCount(10)
        self.__axisX.setFormat('dd.MM.yyyy')
        # noinspection PyUnresolvedReferences
        self.addAxis(self.__axisX, QtCore.Qt.AlignmentFlag.AlignBottom)
        self.__seriesDaily.attachAxis(self.__axisX)
        self.__seriesMean.attachAxis(self.__axisX)
        self.__seriesMedian.attachAxis(self.__axisX)

        self.__axisY = QtCharts.QValueAxis()
        self.__axisY.setTitleText(self.tr('Visits'))
        self.__axisY.setLabelFormat('%d')
        # noinspection PyUnresolvedReferences
        self.addAxis(self.__axisY, QtCore.Qt.AlignmentFlag.AlignLeft)
        self.__seriesDaily.attachAxis(self.__axisY)
        self.__seriesMean.attachAxis(self.__axisY)
        self.__seriesMedian.attachAxis(self.__axisY)
        self.__axisY.setTickCount(10)
        self.__axisY.setMinorTickCount(5)
        self.__axisY.applyNiceNumbers()

    def createSeries(self) -> None:
        values = [v for _, v in self.customData]

        self.__seriesDaily = QtCharts.QLineSeries()
        self.__seriesDaily.append([QtCore.QPointF(float(k), v) for k, v in self.customData])
        self.__seriesDaily.setName(self.tr('Daily'))
        self.__seriesDaily.pen().setColor(QtGui.QColor.fromRgb(33, 150, 243))
        self.__seriesDaily.pen().setWidth(2)
        self.addSeries(self.__seriesDaily)

        self.__seriesMean = QtCharts.QLineSeries()
        self.__seriesMean.append(float(self.customData[0][0]), statistics.mean(values))
        self.__seriesMean.append(float(self.customData[-1][0]), statistics.mean(values))
        self.__seriesMean.setName(self.tr('Mean'))
        self.__seriesMean.pen().setColor(QtGui.QColor.fromRgb(255, 193, 7))
        self.__seriesMean.pen().setWidth(2)
        self.addSeries(self.__seriesMean)

        self.__seriesMedian = QtCharts.QLineSeries()
        self.__seriesMedian.append(float(self.customData[0][0]), statistics.median(values))
        self.__seriesMedian.append(float(self.customData[-1][0]), statistics.median(values))
        self.__seriesMedian.setName(self.tr('Median'))
        self.__seriesMedian.pen().setColor(QtGui.QColor.fromRgb(205, 220, 57))
        self.__seriesMedian.pen().setWidth(2)
        self.addSeries(self.__seriesMedian)

    def setCustomData(self, data: typing.List[typing.Tuple[int, int]]) -> None:
        self.clear()
        self.customData = data
        self.createSeries()
        self.createAxes()
