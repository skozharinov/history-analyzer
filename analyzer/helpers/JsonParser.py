from PySide2 import QtCore

from ..entities import HistoryRecord


class BrowserHistoryRecordFactory:
    @staticmethod
    def createRecord(**kwargs) -> HistoryRecord:
        dateTime = QtCore.QDateTime.fromMSecsSinceEpoch(kwargs['time_usec'] // 1e3)
        transition = HistoryRecord.PageTransition(kwargs['page_transition'])
        faviconUrl = QtCore.QUrl(getattr(kwargs, 'favicon_url', str()))
        url = QtCore.QUrl(kwargs['url'])
        title = kwargs['title']
        return HistoryRecord(transition=transition, dateTime=dateTime, url=url, faviconUrl=faviconUrl, title=title)


class JsonParser(QtCore.QObject):
    # noinspection PyUnresolvedReferences
    started = QtCore.Signal()
    # noinspection PyUnresolvedReferences
    finished = QtCore.Signal()
    # noinspection PyUnresolvedReferences
    progress = QtCore.Signal(int)

    def __init__(self, raw: QtCore.QByteArray, parent: QtCore.QObject = None) -> None:
        super().__init__(parent)
        self.raw = raw
        self.parsed = list()

    def parse(self) -> None:
        self.started.emit()
        self.parsed.clear()
        json = QtCore.QJsonDocument.fromJson(self.raw)

        if 'Browser History' in json.object() and isinstance(json.object()['Browser History'], list):
            size = len(json.object()['Browser History'])

            for i, entry in enumerate(json.object()['Browser History']):
                record = BrowserHistoryRecordFactory.createRecord(**entry)
                self.progress.emit((i + 1) / size * 100)
                if record is not None:
                    self.parsed.append(record)

        self.parsed.sort(key=lambda x: x.dateTime)
        self.finished.emit()
