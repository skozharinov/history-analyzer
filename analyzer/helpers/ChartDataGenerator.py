import typing

from PySide2 import QtCore

from ..entities import HistoryRecord


class ChartDataGenerator(QtCore.QObject):
    # noinspection PyUnresolvedReferences
    started = QtCore.Signal()
    # noinspection PyUnresolvedReferences
    finished = QtCore.Signal()
    # noinspection PyUnresolvedReferences
    progress = QtCore.Signal(int)

    def __init__(self, data: typing.List[HistoryRecord], parent: QtCore.QObject = None) -> None:
        super().__init__(parent)
        self.recordData = data
        self.transitionsChartData = list()
        self.domainsChartData = list()
        self.dailyChartData = list()
        self.hoursChartData = list()
        self.weekdaysChartData = list()
        self.daysChartData = list()
        self.monthsChartData = list()
        self.yearsChartData = list()

    def generate(self) -> None:
        self.started.emit()

        byTransition = dict()
        byDomain = dict()
        byDate = dict()
        byHour = dict()
        byWeekday = dict()
        byDay = dict()
        byMonth = dict()
        byYear = dict()

        recordsTotal = len(self.recordData)
        for i, r in enumerate(self.recordData):
            transition = r.transition
            if transition not in byTransition:
                byTransition[transition] = 1
            else:
                byTransition[transition] += 1

            domain = '.'.join(r.url.host().split('.')[-len(r.url.topLevelDomain().split('.')):])
            if 'chrome' not in r.url.scheme():
                if domain not in byDomain:
                    byDomain[domain] = 1
                else:
                    byDomain[domain] += 1

            dateTimestamp = QtCore.QDateTime(r.dateTime.date()).toMSecsSinceEpoch()
            if dateTimestamp not in byDate:
                byDate[dateTimestamp] = 1
            else:
                byDate[dateTimestamp] += 1

            hour = r.dateTime.time().hour()
            if hour not in byHour:
                byHour[hour] = 1
            else:
                byHour[hour] += 1

            weekday = r.dateTime.date().dayOfWeek()
            if weekday not in byWeekday:
                byWeekday[weekday] = 1
            else:
                byWeekday[weekday] += 1

            day = r.dateTime.date().day()
            if day not in byDay:
                byDay[day] = 1
            else:
                byDay[day] += 1

            month = r.dateTime.date().month()
            if month not in byMonth:
                byMonth[month] = 1
            else:
                byMonth[month] += 1

            year = r.dateTime.date().year()
            if year not in byYear:
                byYear[year] = 1
            else:
                byYear[year] += 1

            self.progress.emit((i + 1) / recordsTotal * 100)

        self.transitionsChartData = list(byTransition.items())
        self.transitionsChartData.sort(key=lambda x: x[1], reverse=True)
        self.domainsChartData = list(byDomain.items())
        self.domainsChartData.sort(key=lambda x: x[1], reverse=True)
        self.dailyChartData = list(byDate.items())
        self.dailyChartData.sort()
        self.hoursChartData = list(byHour.items())
        self.hoursChartData.sort()
        self.weekdaysChartData = list(byWeekday.items())
        self.weekdaysChartData.sort()
        self.daysChartData = list(byDay.items())
        self.daysChartData.sort()
        self.monthsChartData = list(byMonth.items())
        self.monthsChartData.sort()
        self.yearsChartData = list(byYear.items())
        self.yearsChartData.sort()

        self.finished.emit()
