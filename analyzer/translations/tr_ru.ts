<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="1.1" language="ru_RU">
    <context>
        <name>AboutDialog</name>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="11"/>
            <source>History Analyzer</source>
            <translation>History Analyzer</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="15"/>
            <source>Copyright (c) 2019 Sergey Kozharinov</source>
            <translation>Copyright (c) 2019 Sergey Kozharinov</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="20"/>
            <source>&lt;h1&gt;&lt;b&gt;%s&lt;/b&gt;&lt;/h1&gt;</source>
            <translation>&lt;h1&gt;&lt;b&gt;%s&lt;/b&gt;&lt;/h1&gt;</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="24"/>
            <source>&lt;h4&gt;%s&lt;/h4&gt;</source>
            <translation>&lt;h4&gt;%s&lt;/h4&gt;</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="28"/>
            <source>Version: %s</source>
            <translation>Версия: %s</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="44"/>
            <source>About</source>
            <translation>Справка</translation>
        </message>
        <message>
            <location filename="../dialogs/AboutDialog.py" line="12"/>
            <source>Plotter for Google Chrome browser history data</source>
            <translation>Программа, рисующая графики на основе истории браузера Google Chrome</translation>
        </message>
    </context>
    <context>
        <name>DailyChart</name>
        <message>
            <location filename="../charts/DailyChart.py" line="60"/>
            <source>Daily</source>
            <translation>Ежедневно</translation>
        </message>
        <message>
            <location filename="../charts/DailyChart.py" line="44"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/DailyChart.py" line="68"/>
            <source>Mean</source>
            <translation>Среднее</translation>
        </message>
        <message>
            <location filename="../charts/DailyChart.py" line="76"/>
            <source>Median</source>
            <translation>Медиана</translation>
        </message>
    </context>
    <context>
        <name>DaysChart</name>
        <message>
            <location filename="../charts/DaysChart.py" line="16"/>
            <source>Days</source>
            <translation>Дни</translation>
        </message>
        <message>
            <location filename="../charts/DaysChart.py" line="35"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/DaysChart.py" line="46"/>
            <source>Total</source>
            <translation>Всего</translation>
        </message>
    </context>
    <context>
        <name>DomainsChart</name>
        <message>
            <location filename="../charts/DomainsChart.py" line="15"/>
            <source>Domains</source>
            <translation>Сайты</translation>
        </message>
        <message>
            <location filename="../charts/DomainsChart.py" line="28"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
    </context>
    <context>
        <name>ExportDialog</name>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="13"/>
            <source>Output file path</source>
            <translation>Выходной файл</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="16"/>
            <source>Select</source>
            <translation>Выбрать</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="23"/>
            <source>File</source>
            <translation>Файл</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="32"/>
            <source>Chart</source>
            <translation>График</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="38"/>
            <source>Width, px</source>
            <translation>Ширина, px</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="47"/>
            <source>Height, px</source>
            <translation>Высота, px</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="56"/>
            <source>Size</source>
            <translation>Размер</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="68"/>
            <source>Export</source>
            <translation>Экспортировать</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="93"/>
            <source>Images (*.jpeg, *.jpg, *.png)</source>
            <translation>Картинки (*.jpeg, *.jpg, *.png)</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="93"/>
            <source>Export Chart</source>
            <translation>Экспорт графика</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="114"/>
            <source>Exporting...</source>
            <translation>Экспортируется...</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="117"/>
            <source>Exported successfully!</source>
            <translation>Экспортировано успешено!</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="119"/>
            <source>Export failed</source>
            <translation>Ошибка при экспорте</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="59"/>
            <source>Chart antialiasing</source>
            <translation>Сглаживание графика</translation>
        </message>
        <message>
            <location filename="../dialogs/ExportDialog.py" line="65"/>
            <source>Options</source>
            <translation>Параметры</translation>
        </message>
    </context>
    <context>
        <name>HistoryTableModel</name>
        <message>
            <location filename="../models/HistoryTableModel.py" line="48"/>
            <source>Time visited</source>
            <translation>Время</translation>
        </message>
        <message>
            <location filename="../models/HistoryTableModel.py" line="50"/>
            <source>URL</source>
            <translation>URL</translation>
        </message>
        <message>
            <location filename="../models/HistoryTableModel.py" line="53"/>
            <source>Title</source>
            <translation>Заголовок</translation>
        </message>
    </context>
    <context>
        <name>HoursChart</name>
        <message>
            <location filename="../charts/HoursChart.py" line="16"/>
            <source>Hours</source>
            <translation>Часы</translation>
        </message>
        <message>
            <location filename="../charts/HoursChart.py" line="35"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/HoursChart.py" line="46"/>
            <source>Total</source>
            <translation>Всего</translation>
        </message>
    </context>
    <context>
        <name>MainWindow</name>
        <message>
            <location filename="../MainWindow.py" line="91"/>
            <source>Open</source>
            <translation>Открыть файл</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="99"/>
            <source>Export chart</source>
            <translation>Экспортировать график</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="95"/>
            <source>Close</source>
            <translation>Закрыть файл</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="115"/>
            <source>Exit</source>
            <translation>Выйти</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="120"/>
            <source>About %s</source>
            <translation>О %s</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="123"/>
            <source>About Qt</source>
            <translation>О Qt</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="195"/>
            <source>File</source>
            <translation>Файл</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="208"/>
            <source>Help</source>
            <translation>Справка</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="245"/>
            <source>Open Takeout file</source>
            <translation>Открыть файл данных</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="245"/>
            <source>JSON (*.json)</source>
            <translation>JSON (*.json)</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="341"/>
            <source>Oops!</source>
            <translation>Упс!</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="342"/>
            <source>Failed to load your data</source>
            <translation>Не удалось загрузить Ваши данные</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="392"/>
            <source>%d records in this file</source>
            <translation>%d записей в этом файле</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="404"/>
            <source>Parsing...</source>
            <translation>Открываем файл...</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="142"/>
            <source>1. Go to &lt;a href=&quot;https://takeout.google.com&quot;&gt;takeout.google.com&lt;/a&gt;&lt;br&gt;2. Leave only &lt;i&gt;Chrome&lt;/i&gt; data checked&lt;br&gt;3. Leave only &lt;i&gt;BrowserHistory&lt;/i&gt; format checked in &lt;i&gt;Chrome&lt;/i&gt; data&lt;br&gt;4. Extract &lt;b&gt;BrowserHistory.json&lt;/b&gt; from received archive&lt;br&gt;5. Open &lt;b&gt;BrowserHistory.json&lt;/b&gt; with this app</source>
            <translation>1. Пройдите по ссылке: &lt;a href=&quot;https://takeout.google.com&quot;&gt;takeout.google.com&lt;/a&gt;&lt;br&gt;2. Оставьте отмеченной только категорию &lt;i&gt;Chrome&lt;/i&gt;&lt;br&gt;3. Оставьте отмеченным только формат &lt;i&gt;BrowserHistory&lt;/i&gt; в категории &lt;i&gt;Chrome&lt;/i&gt;&lt;br&gt;4. Извлеките файл &lt;b&gt;BrowserHistory.json&lt;/b&gt; из полученного арзива&lt;br&gt;5. Откройте файл &lt;b&gt;BrowserHistory.json&lt;/b&gt; с помошью данной программы</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="102"/>
            <source>Show charts</source>
            <translation>Показывать графики</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="106"/>
            <source>Show history</source>
            <translation>Показывать историю</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="204"/>
            <source>View</source>
            <translation>Вид</translation>
        </message>
        <message>
            <location filename="../MainWindow.py" line="416"/>
            <source>Processing charts...</source>
            <translation>Генерируем графики...</translation>
        </message>
    </context>
    <context>
        <name>MonthsChart</name>
        <message>
            <location filename="../charts/MonthsChart.py" line="16"/>
            <source>Months</source>
            <translation>Месяца</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="37"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="48"/>
            <source>Total</source>
            <translation>Всего</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>Jan</source>
            <translation>Янв</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>Feb</source>
            <translation>Фев</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>Mar</source>
            <translation>Мар</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>Apr</source>
            <translation>Апр</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>May</source>
            <translation>Май</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="29"/>
            <source>Jun</source>
            <translation>Июн</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="30"/>
            <source>Jul</source>
            <translation>Июл</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="30"/>
            <source>Aug</source>
            <translation>Авг</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="30"/>
            <source>Sep</source>
            <translation>Сен</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="30"/>
            <source>Oct</source>
            <translation>Окт</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="30"/>
            <source>Nov</source>
            <translation>Ноя</translation>
        </message>
        <message>
            <location filename="../charts/MonthsChart.py" line="31"/>
            <source>Dec</source>
            <translation>Дек</translation>
        </message>
    </context>
    <context>
        <name>QCoreApplication</name>
        <message>
            <location filename="../__main__.py" line="23"/>
            <source>History Analyzer</source>
            <translation>History Analyzer</translation>
        </message>
    </context>
    <context>
        <name>TransitionsChart</name>
        <message>
            <location filename="../charts/TransitionsChart.py" line="16"/>
            <source>Transitions</source>
            <translation>Типы переходов</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="29"/>
            <source>Link</source>
            <translation>Ссылка</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="31"/>
            <source>Typed</source>
            <translation>Набор</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="33"/>
            <source>Auto Bookmark</source>
            <translation>Предложение</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="35"/>
            <source>Auto Subframe</source>
            <translation>Субфрейм (автоматически)</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="37"/>
            <source>Manual Subframe</source>
            <translation>Субфрейм (вручную)</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="39"/>
            <source>Omnibar Generated</source>
            <translation>Генерация</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="41"/>
            <source>Top Level</source>
            <translation>Высший уровень</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="43"/>
            <source>Form Submit</source>
            <translation>Отправка формы</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="45"/>
            <source>Reload</source>
            <translation>Перезагрузка</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="47"/>
            <source>Keyword</source>
            <translation>Ключевое слово</translation>
        </message>
        <message>
            <location filename="../charts/TransitionsChart.py" line="49"/>
            <source>Keyword Generated</source>
            <translation>Ключевое слово (сгенерировано)</translation>
        </message>
    </context>
    <context>
        <name>WeekdaysChart</name>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="16"/>
            <source>Weekdays</source>
            <translation>Дни недели</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="37"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="48"/>
            <source>Total</source>
            <translation>Всего</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Mon</source>
            <translation>Пн</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Tue</source>
            <translation>Вт</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Wed</source>
            <translation>Ср</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Thu</source>
            <translation>Чт</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Fri</source>
            <translation>Пт</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="29"/>
            <source>Sat</source>
            <translation>Сб</translation>
        </message>
        <message>
            <location filename="../charts/WeekdaysChart.py" line="31"/>
            <source>Sun</source>
            <translation>Вс</translation>
        </message>
    </context>
    <context>
        <name>YearsChart</name>
        <message>
            <location filename="../charts/YearsChart.py" line="16"/>
            <source>Years</source>
            <translation>Годы</translation>
        </message>
        <message>
            <location filename="../charts/YearsChart.py" line="35"/>
            <source>Visits</source>
            <translation>Посещения</translation>
        </message>
        <message>
            <location filename="../charts/YearsChart.py" line="46"/>
            <source>Total</source>
            <translation>Всего</translation>
        </message>
    </context>
</TS>
