SOURCES = ../__main__.py \
          ../MainWindow.py \
          ../charts/DailyChart.py \
          ../charts/DomainsChart.py \
          ../charts/WeekdaysChart.py \
          ../charts/DaysChart.py \
          ../charts/HoursChart.py \
          ../charts/MonthsChart.py \
          ../charts/TransitionsChart.py \
          ../charts/YearsChart.py \
          ../dialogs/AboutDialog.py \
          ../dialogs/ExportDialog.py \
          ../models/HistoryTableModel.py
TRANSLATIONS = ../translations/tr_ru.ts
