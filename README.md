# History Analyzer
Plotter for Google Chrome synced browser history

###  Requirements
* [Python 3.7+](https://www.python.org/)
* [Qt 5.12+](https://www.qt.io/developers/)

### Compilation
```bash
git clone https://gitlab.com/skozharinov/history-analyzer.git
pip -r history-analyzer/requirements.txt
lrelease history-analyzer/analyzer/translations/tr.pro
```

### Usage
```bash
cd history-analyzer
python -m analyzer
```

### Examples
<img src="examples/transitions.png" alt="Transitions Pie Chart" width="49.5%"/>
<img src="examples/domains.png" alt="Domains Bar Chart" width="49.5%"/>
<img src="examples/daily.png" alt="Daily Line Chart" width="49.5%"/>
<img src="examples/hours.png" alt="Hours Bar Chart" width="49.5%"/>
<img src="examples/weekdays.png" alt="Weekdays Bar Chart" width="49.5%"/>
<img src="examples/days.png" alt="Days Bar Chart" width="49.5%"/>
<img src="examples/months.png" alt="Months Bar Chart" width="49.5%"/>
<img src="examples/years.png" alt="Years Bar Chart" width="49.5%"/>

